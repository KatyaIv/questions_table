<?php

 	class SeminarModule
    {
        function startingSeminar( $type, $entity )
        {
            global $link;

            $query = "INSERT INTO scenario (status, type, entity) VALUES('0', '{$type}', '{$entity}');";

            if($link->query($query))
            {

                $answer['status'] = 'OK';
                $answer['message'] = 'Seminar started';
                echo json_encode($answer);

            } else {

                $answer['status'] = 'ERROR';
                $answer['message'] = 'Seminar is not running';
                echo json_encode($answer);
            } 
        }

        function endingSeminar()
        {
            global $link;

            $query = "UPDATE scenario SET status='1' WHERE status='0';";

            if($link->query($query))
            {
                $answer['status'] = 'OK';
                $answer['message'] = 'Seminar ended';
                echo json_encode($answer);
            } else {
                $answer['status'] = 'ERROR';
                $answer['message'] = 'Seminar is not ending';
                echo json_encode($answer);
            }
        }

        function checkStartedSeminar()
        {
            global $link;

            $query = "SELECT id,type,entity FROM scenario WHERE status='0';";

            if ($result = $link->query($query)) {

                if ($rowList = $result->fetch_assoc()) {

                    $answer['status'] = 'OK';
                    $answer['message'] = 'Started seminar found';
                    $answer['id'] = $rowList['id'];
                    $answer['type'] = $rowList['type'];
                    $answer['entity'] = $rowList['entity'];
                        

                } else {

                    $answer['message'] = 'Started seminar not found';
 
                }

                $result->free();
            }

            echo json_encode($answer);
        }

        function insertStudentAnswer( $userId, $questionId, $answerId, $seminarId )
        {
            global $link;

            $rowList = AnswerModule::getAllByQuestionID( $questionId );

            $query = "INSERT INTO students_answers (user_id, question_id, answer_id, scenario_id) VALUES('{$userId}', '{$questionId}', 
                    '{$rowList[$answerId]['id']}', '{$seminarId}');";

            if($link->query($query))
            {
                $answer['status'] = 'OK';
                $answer['message'] = 'Insert student answer complete';

                echo json_encode($answer);
            } else {

                $answer['status'] = "ERROR";
                $answer['message'] = "Insert student answer not complete";

                echo json_encode($answer);

            }
                  
        }

        function getIdSeminars()
        {
            global $link;

            $query = 'SELECT id,entity FROM scenario WHERE status="1";';

            if ($result = $link->query($query)) {

                while ($row = $result->fetch_assoc()) {
                        $rowList[] = $row;
                }

                $result->free();
            }

            return $rowList;
        
        }
    }
?>