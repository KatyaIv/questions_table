define( function(){
	var _tableTemplate = function(_responseTable){
		var _tableForm = $('<form role="form" class="formTable"><br></form>');
        var _divContainer = $('<div class="container"/>');
        var _table = $('<table class="table table-striped table-bordered table-hover"/>');
        var _tableBody = $('<tbody></tbody>');

        _table.append(_tableBody);
        _divContainer.append(_table);
		_tableForm.append(_divContainer);
                                   
        return _tableForm;
	};

	return _tableTemplate;
});
