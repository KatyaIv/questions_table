define(function () {
    var _navBarTemplate = function () {
        var _navBarMain = $('<nav class="navbar-main navbar-inverse" role="navigation" />');
        var _navContainer = $('<nav class="container" />');
        var _ulNavBar = $('<ul class="nav navbar-nav" />');
        var _ulNavBarRight = $('<ul class="nav navbar-nav navbar-right" />');

        _ulNavBar.append([
            '<li class="dropdown">' +
            '<a href="#" class="dropdown-toggle scenario" data-toggle="dropdown">Начало тестирования</a>' +
            '<ul class="dropdown-menu" role="menu">' +
            '<li><a class="btn several-subjects">Все вопорсы по нескольким темам</a></li>' +
            '<li><a class="btn one-subject">Все вопросы по одной теме</a></li>' +
            '</ul>' +
            '</li>' +
            '<li><a class="btn get-statistic">Статистика</a></li>' +
            '<li class="dropdown">' +
            '<a href="#" class="dropdown-toggle get-table" data-toggle="dropdown">Просмотр вопросов</a>' +
            '<ul class="dropdown-menu" role="menu">' +
            '<li><a class="btn get-question">Вопросы</a></li>' +
            '<li><a class="btn get-subj">Темы</a></li>' +
            '</ul>' +
            '</li>'
        ]);

        _ulNavBarRight.append([
            '<li><p class="navbar-text"><i class="glyphicon glyphicon-user"></i></p><li>' +
            '<li class="dropdown">' +
            '<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-menu-hamburger"></i></a>' +
            '<ul class="dropdown-menu" role="menu">' +
            '<li><a class="btn disabled add-question"><i class="glyphicon glyphicon-plus"></i>Добавить новый вопрос</a></li>' +
            '<li><a class="btn disabled add-subj"><i class="glyphicon glyphicon-plus"></i>Добавить новую тему</a></li>' +
            '<li><a class="btn disabled edit-question"><i class="glyphicon glyphicon-pencil"></i>Редактировать вопросы</a></li>' +
            '<li><a class="btn disabled edit-subj"><i class="glyphicon glyphicon-pencil"></i>Редактировать темы</a></li>' +
            '<li><a class="btn disabled del-question"><i class="glyphicon glyphicon-trash"></i>Удалить вопрос</a></li>' +
            '<li><a class="btn disabled del-subj"><i class="glyphicon glyphicon-trash"></i>Удалить тему</a></li>' +
            '<li role="separator" class="divider"></li>' +
            '<li><a class="btn exit"><i class="glyphicon glyphicon-off"></i>Выход</a></li>' +
            '</ul>' +
            '</li>'
        ]);

        _navContainer.append([
            _ulNavBar,
            _ulNavBarRight
        ]);

        _navBarMain.append(_navContainer);

        return _navBarMain;
    };

    return _navBarTemplate;
});
