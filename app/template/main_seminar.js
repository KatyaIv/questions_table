define( function() {
	var _checkSeminarTemplate = function() {
		var _checkSeminarForm = $('<form role="form" class="del form-inline"/>');
        var _divRowCheck = $('<div class="row"/>');
        var _divColCheck = $('<div class="col-lg-offset-4 col-lg-4 col-lg-offset-4"/>');
        var _inputEntity = $('<input type="text" class="input-medium form-control" id="main_seminar-inputEntity" autocomplete="off"/>');
        var _btnOk = $('<button type="submit" class="btn btn-inverse"><i class="glyphicon glyphicon-ok"></i></button>');

        _divColCheck.append([
            '<label class="control-label" for="main_seminar-inputEntity"></label>',
            _inputEntity,
            _btnOk
        ]);

        _divRowCheck.append(_divColCheck);

        _checkSeminarForm.append([
            '<h4 align="center"></h4><br>',
            _divRowCheck,
            '<br>'
        ]);

        return _checkSeminarForm;
	};

	return _checkSeminarTemplate;
});