/**
 * Created by Катя on 04-Apr-16.
 */

define(function () {
    var _authTemplate = function () {
        var _formAuth = $('<form role="form" class="auth"/>');
        var _divRowLogin = $('<div class="row form-group"/>');
        var _divColLogin = $('<div class=" col-lg-offset-4 col-lg-4 col-lg-offset-4"/>');
        var _divRowPass = $('<div class="row form-group"/>');
        var _divColPass = $('<div class=" col-lg-offset-4 col-lg-4 col-lg-offset-4"/>');
        var _divRowBtn = $('<div class="row form-group"/>');
        var _divColBtn = $('<div class=" col-lg-offset-4 col-lg-4 col-lg-offset-4"/>');
        var _divRowError = $('<div class="row form-group"/>');
        var _divColError = $('<div class=" col-lg-offset-4 col-lg-4 col-lg-offset-4"/>');
        var _inputLogin = $('<input type="text" class="form-control input-lg" id="login" placeholder="Введите логин">');
        var _inputPassword = $('<input type="password" class="form-control input-lg" id="pass" placeholder="Введите пароль">');
        var _btnEnter = $('<button type="submit" class="btn btn-inverse">Войти</button>');
        var _btnSignUp = $('<button type="button" class="btn btn-link">Регистрация</button>');
        var _divNotification = $('<div class="alert alert-danger" role="alert" hidden="true">Не верный логин или пароль!</div>');

        _divColLogin.append([
            '<label for="login">Логин</label>',
            _inputLogin
        ]);

        _divColPass.append([
            '<label for="pass">Пароль</label>',
            _inputPassword
        ]);

        _divColBtn.append([
            _btnEnter,
            _btnSignUp
        ]);

        _divColError.append([
            '<br>',
            _divNotification
        ]);

        _divRowLogin.append(_divColLogin);
        _divRowPass.append(_divColPass);
        _divRowBtn.append(_divColBtn);
        _divRowError.append(_divColError);

        _formAuth.append([
            '<legend align="center">Авторизация</legend>',
            _divRowLogin,
            _divRowPass,
            _divRowBtn,
            _divRowError
        ]);

        return _formAuth;
    };

    return _authTemplate;
});
