define(function () {
    var _delTemplate = function () {
        var _delForm = $('<form role="form" class="del form-inline"/>');
        var _divRowDel = $('<div class="row"/>');
        var _divColDel = $('<div class="col-lg-offset-4 col-lg-4 col-lg-offset-4"/>');
        var _inputNum = $('<input type="text" class="input-medium form-control" id="main-del-inputNum" autocomplete="off"/>');
        var _btnDel = $('<button type="submit" class="btn btn-inverse"><i class="glyphicon glyphicon-trash"></i></button>');

        _divColDel.append([
            '<label class="control-label" for="main-del-inputNum"></label>',
            _inputNum,
            _btnDel
        ]);

        _divRowDel.append(_divColDel);
        
        _delForm.append([
            '<h4 align="center"></h4><br>',
            _divRowDel,
            '<br>'
        ]);

        return _delForm;
    };

    return _delTemplate;
});
