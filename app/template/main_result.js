define(function () {
	var _resultTemplate = function() {
		var _resultForm = $('<form role="form" class="result"/>');
		var _divRowResult = $('<div class="row"/>');
        var _divColResult = $('<div class="col-lg-6 col-lg-offset-3"/>');

        _divRowResult.append(_divColResult);

        _resultForm.append([
        	'<h4 align="center"></h4><br>',
        	_divRowResult
        ]);

        return _resultForm;
	};

	return _resultTemplate;
});