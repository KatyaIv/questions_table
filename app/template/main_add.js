define(function () {
    var _addTemplate = function () {
        var _addForm = $('<form role="form" class="add"/>');
        var _divRowAdd = $('<div class="row"/>');
        var _divColAdd = $('<div class="col-lg-6 col-lg-offset-3"/>');
        var _inputSubject = $('<input type="text" class="input-medium form-control" id="main-add-inputSubject" autocomplete="off"/>');
        var _textArea = $('<textarea class="form-control" rows="3"></textarea>');
        var _btnAdd = $('<button type="submit" class="btn btn-inverse btn-block"><i class="glyphicon glyphicon-plus"></i></button>');
        var _inputOptionA = $('<input type="text" class="input-medium form-control" placeholder="Вариант ответа А" autocomplete="off"/>');
        var _inputOptionB = $('<input type="text" class="input-medium form-control" placeholder="Вариант ответа Б" autocomplete="off"/>');
        var _inputOptionC = $('<input type="text" class="input-medium form-control" placeholder="Вариант ответа В" autocomplete="off"/>');
        var _inputOptionD = $('<input type="text" class="input-medium form-control" placeholder="Вариант ответа Г" autocomplete="off"/>');
        var _radioOptionA = $('<label class="radio-inline"><input type="radio" name="optionsAnswers" value=0> А </label></input>');
        var _radioOptionB = $('<label class="radio-inline"><input type="radio" name="optionsAnswers" value=1> Б </label></input>');
        var _radioOptionC = $('<label class="radio-inline"><input type="radio" name="optionsAnswers" value=2> В </label></input>');
        var _radioOptionD = $('<label class="radio-inline"><input type="radio" name="optionsAnswers" value=3> Г </label></input>');

        _divColAdd.append([
            '<label class="control-label" for="main-auth-inputText"></label>',
            _inputSubject,
            _textArea,
            '<br><br>',
            _inputOptionA,
            _inputOptionB,
            _inputOptionC,
            _inputOptionD,
            '<p>Правильный вариант ответа: </p>',
            _radioOptionA,
            _radioOptionB,
            _radioOptionC,
            _radioOptionD,
            _btnAdd        
        ]);
 
        _divRowAdd.append(_divColAdd);
        _addForm.append([
            '<h4 align="center"></h4><br>',
            _divRowAdd,
            '<br>'
        ]);

        return _addForm;
    };

    return _addTemplate;
});
