/**
 * Created by Катя on 05-Apr-16.
 */

define(function () {
    var _signUpTemplate = function () {
        var _formSignUp = $('<form role="form" class="reg"/>');
        var _divRowLogin = $('<div class="row form-group"/>');
        var _divRowFullName = $('<div class="row form-group"/>');
        var _divRowPass = $('<div class="row form-group"/>');
        var _divRowRole = $('<div class="row form-group"/>');
        var _inputLogin = $('<input type="text" class="form-control input-lg" id="login" autocomplete="off">');
        var _inputFullName = $('<input type="text" class="form-control input-lg" id="full_name" autocomplete="off">');
        var _inputPassword = $('<input type="password" class="form-control input-lg" id="pass">');
        var _btnSignUp = $('<button type="submit" class="btn btn-inverse">Зарегестрироваться</button>');
        var _divError = $('<div class="alert alert-danger" role="alert" hidden="true"></div>')

        _divRowLogin.append([
            '<div class="col-lg-1 col-lg-offset-3">' +
            '<label for="login">Логин</label></div>',
            '<div class="col-lg-4"/>'
        ]);

        _divRowLogin.find('.col-lg-4').append(_inputLogin);

        _divRowFullName.append([
            '<div class="col-lg-1 col-lg-offset-3">' +
            '<label for="login">ФИО</label></div>' +
            '<div class="col-lg-4"/>'
        ]);

        _divRowFullName.find('.col-lg-4').append(_inputFullName);

        _divRowPass.append([
            '<div class="col-lg-1 col-lg-offset-3">' +
            '<label for="login">Пароль</label></div>' +
            '<div class="col-lg-4"/>'
        ]);

        _divRowPass.find('.col-lg-4').append(_inputPassword);

        _divRowRole.append([
            '<div class="col-lg-1 col-lg-offset-3">' +
            '<label for="login">Права доступа</label></div>' +
            '<div class="col-lg-4"/>'
        ]);

        _divRowRole.find('.col-lg-4').append([
            '<select class="form-control">' +
            '<option value="0">студент</option>' +
            '<option value="1">преподаватель</option>' +
            '</select><br>',
            _btnSignUp,
            '<br><br>',
            _divError
        ]);

        _formSignUp.append([
            '<legend align="center">Регистрация</legend>',
            _divRowFullName,
            _divRowLogin,
            _divRowPass,
            _divRowRole
        ]);

        return _formSignUp;
    };
    return _signUpTemplate;
});