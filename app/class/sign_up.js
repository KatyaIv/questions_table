/**
 * Created by Катя on 05-Apr-16.
 */

define(
    ['../template/main_sign_up','class/client'],
    function (_signUpTemplate, _clientModule) {

        var _signUpModule = (function () {
            var signUp = {
                client: false,
                formSignUp: false,
                inputLogin: false,
                inputFullName: false,
                inputPassword: false,
                selectRole: false,
                userData: {
                    login: '',
                    password: '',
                    fullName: '',
                    role: null
                }
            };

            signUp.init = function () {

                signUp.formSignUp = _signUpTemplate();
                signUp.client = _clientModule();

                if (signUp.formSignUp.length == 0) return false;

                $('body').prepend(signUp.formSignUp);

                signUp.inputLogin = $(signUp.formSignUp.find('#login'));
                signUp.inputFullName = $(signUp.formSignUp.find('#full_name'));
                signUp.inputPassword = $(signUp.formSignUp.find('#pass'));
                signUp.selectRole = $(signUp.formSignUp.find('select'));

                signUp.attachEvent_SignUp();

                return true;
            };

            signUp.userDataCollect = function () {

                signUp.userData.login = signUp.inputLogin.val();
                signUp.userData.password = signUp.inputPassword.val();
                signUp.userData.fullName = signUp.inputFullName.val();

                if (signUp.userData.login == '' || signUp.userData.password == '' || signUp.userData.fullName == '') {
                    return false;
                }

                signUp.userData.role = signUp.selectRole.val();

                return true;
            };

            signUp.saveUserToDB = function () {

                var _data = {
                        query: 'reg_user',
                        login: signUp.userData.login,
                        password: signUp.userData.password,
                        fullName: signUp.userData.fullName,
                        role: signUp.userData.role
                };
                
                signUp.client.post(_data, function(){
                
                    if(signUp.client.responseJson.status == "OK"){

                            signUp.formSignUp.detach();
                            $('body').append('Регистрация успешно завершена! Для продолжения обновите страницу.');

                        } else {

                            if(signUp.client.responseJson.message == "Login busy"){

                                signUp.formSignUp.find('.alert').text('Логин уже существует!').show();

                            } else {

                                console.log(signUp.client.responseJson);

                            }
                        }

                });
                                         
            };

            signUp.attachEvent_SignUp = function () {

                signUp.formSignUp.submit(function () {

                    signUp.formSignUp.find('.alert').hide();

                    if (signUp.userDataCollect()) {

                        signUp.saveUserToDB();

                    } else {

                        signUp.formSignUp.find('.alert').text('Заполните все поля!').show();

                    }

                    return false;

                });
            };

            return signUp;

        });
        
        return _signUpModule;

    });