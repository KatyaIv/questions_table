define(
    ['class/client', 'class/table'],
    function (_clientModule, _tableModule) {

    var _subjModule = function () {

        var subj = {
            client: false,
            mainCont: false,
            init: function () {
                this.client = _clientModule();
            }
        };

        subj.getTable = function () {

            var _data  = {
                    query: 'get_table_subj'
            };
            
            subj.client.post(_data, function () {

                subj.table = _tableModule();

                subj.table.constructionSubjects(subj.client.responseJson);

            });

        };

        subj.addSubject = function (_subject, callback) {

            var _data = {
                    query: 'insert_subj',
                    subject: _subject

            };
            
            subj.client.post(_data, callback);

        };

        subj.delSubject = function (_idSubject, callback) {

            var _data = {
                    query: 'del_subj',
                    id: _idSubject
            };
            
            subj.client.post(_data, callback);    

        };

        subj.editSubject = function (_id, _name, callback) {

            var _data = {
                    query: 'edit_subj',
                    id: _id,
                    name: _name
            };

            subj.client.post(_data, callback);

        };


        subj.init();

        return subj;
    };

    return _subjModule;
});