define(function () {

    	var _clientModule = (function () {
            var client = {
                url: "server.php",
                responseJson: null,
            }

            client.post = function(_data, callbackFunc) {
            	$.ajax({
                    url: client.url,
                    method: "POST",
                    data: _data,
                    dataType: 'json',

                    complete: function (response) {
                        client.responseJson = JSON.parse(response.responseText);
                        console.log(client.responseJson);
                        if (callbackFunc !== undefined) {
    						callbackFunc();
  						}
                        
                    }
                });
            };

        	return client;   
        });
    return _clientModule;
	});
