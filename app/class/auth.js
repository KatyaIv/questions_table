/**
 * Created by Катя on 04-Apr-16.
 */

define(
    ['../template/main_auth','class/client'],
    function (_authTemplate, _clientModule) {

        var _authModule = (function () {
            var auth = {
                formAuth: false,
                inputLogin: false,
                inputPassword: false,
                btnSignUp: false,
                registration: false,
                enter: false,
                userRole: null,
                userFullName: null,
                client: false
            };

            auth.init = function () {
                auth.formAuth = _authTemplate();
                auth.client = _clientModule();
                
                if (auth.formAuth.length == 0) return false;

                $('body').append(auth.formAuth);
                auth.inputLogin = $(auth.formAuth.find(':input[placeholder="Введите логин"]'));

                auth.inputPassword = $(auth.formAuth.find(':input[type="password"]'));
                auth.btnSignUp = $(auth.formAuth.find('.btn-link'));

                auth.attachEvent_Enter();
                auth.attachEvent_SignUp();

                return true;
            };

            auth.onRegistration = function (callback) {
                auth.registration = callback;
            };

            auth.onEnter = function (callback) {
                auth.enter = callback;
            };

            auth.userVerification = function (_login, _password) {

                var _data = {
                        query: 'user_verif',
                        login: _login,
                        password: _password
                };

                auth.client.post(_data, function(){
                
                    if(auth.client.responseJson.status == "OK"){

                        auth.userRole = auth.client.responseJson.role;
                        auth.userFullName = auth.client.responseJson.full_name;

                        auth.formAuth.detach();

                        auth.enter();

                    } else {

                        auth.formAuth.find('.alert').show();

                    }

                });

            };

            auth.attachEvent_Enter = function () {

                auth.formAuth.submit(function () {

                    var _login = auth.inputLogin.val();
                    var _password = auth.inputPassword.val();

                    auth.userVerification(_login, _password);

                    return false;

                });

            };

            auth.attachEvent_SignUp = function () {

                auth.btnSignUp.on('click', function () {

                    auth.formAuth.detach();

                    auth.registration();
                    
                });

            };

            return auth;
        });
        return _authModule;
    });

