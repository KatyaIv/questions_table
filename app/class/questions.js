/**
 * Created by Катя on 05-Apr-16.
 */

define(
    ['class/client','class/table'],
    function (_clientModule,_tableModule) {

    var _questionModule = function () {

        var self = {
            client: false,
            mainCont: false,
            table: false,
            init: function()
            {
                this.client = _clientModule();
            }
        };

        self.getTable = function (_subject, _type) {

            var _data = {
                query: 'get_table_questions',
                subject: _subject
            };
            
            self.client.post(_data, function () {

                if(self.client.responseJson.status == 'OK'){

                    self.table = _tableModule();

                    self.table.constructionQuestions(self.client.responseJson, _type);
                } 
                
            });

        };

        self.addQuestion = function (_question, _subject, _answerA, _answerB, _answerC, _answerD, _numCorrectAnswer, callback) {

            var _data = {
                    query: 'insert_question',
                    question: _question,
                    subject: _subject,
                    answerA: _answerA,
                    answerB: _answerB,
                    answerC: _answerC,
                    answerD: _answerD,
                    numCorrectAnswer: _numCorrectAnswer
            };

            self.client.post(_data, callback);

        };

        self.delQuestion = function (_idQuestion, _subject, callback) {

            var _data = {
                    query: 'del_question',
                    id: _idQuestion,
                    subject: _subject
            };

            self.client.post(_data, callback);

        };

        self.editQuestion = function (_id, _text, _subject, _answerA, _answerB, _answerC, _answerD, _numCorrectAnswer, callback) {

            var _data = {
                    query: 'edit_question',
                    id: _id,
                    text: _text,
                    subject: _subject,
                    answerA: _answerA,
                    answerB: _answerB,
                    answerC: _answerC,
                    answerD: _answerD,
                    numCorrectAnswer: _numCorrectAnswer
            };
            
            self.client.post(_data, callback);    

        };

        self.init();

        return self;
    };

    return _questionModule;
});