define(
    ['class/client','../template/main_table'],
    function (_clientModule, _tableTemplate) {

    var _tableModule = function () {

        var table = {
            client: false,
            mainCont: false,
            init: function()
            {
                this.client = _clientModule();
            }
        };

        table.constructionQuestions = function(response, type){

            table.mainCont = _tableTemplate();

            var _divContainer = table.mainCont.find('div');
            var _table = table.mainCont.find('table');
            var _tableBody = table.mainCont.find('tbody');

           for (var i = 0; i < response.table.length; i++) {

                var _numRow = i + 1;
                _tableBody.append('<tr id="table-question-row-' + response.table[i].id +'"><td rowspan="3"><strong>' + _numRow + 
                    '</strong></td><td colspan="4"><strong>' + response.table[i].text + '</strong></td></tr>' + 
                    '<tr><td width="210px">А</td><td width="210px">Б</td><td width="210px">В</td><td>Г</td></tr>' + 
                    '<tr id="table-answer-row-' + response.table[i].id +'"></tr>');

                var _data = {
                    query: 'get_answers',
                    question: response.table[i].id
                };

                table.client.post(_data, function () {
                            
                    if(table.client.responseJson.status == 'OK'){

                        _tableBody.find('#table-answer-row-' + table.client.responseJson.answers[0].question_id ).append( 
                            '<td>' + table.client.responseJson.answers[0].title + '</td>' +
                            '<td>' + table.client.responseJson.answers[1].title + '</td>' +
                            '<td>' + table.client.responseJson.answers[2].title + '</td>' +
                            '<td>' + table.client.responseJson.answers[3].title + '</td>');

                    }
                });

                if(type == 'seminar'){

                    var _td = $('<td rowspan="3" width="200px">');
                    var _radioOptionA = $('<label class="radio-inline"><input type="radio" name="optionsAnswers-' + response.table[i].id + 
                        '" value=0> А </label></input>');
                    var _radioOptionB = $('<label class="radio-inline"><input type="radio" name="optionsAnswers-' + response.table[i].id + 
                        '" value=1> Б </label></input>');
                    var _radioOptionC = $('<label class="radio-inline"><input type="radio" name="optionsAnswers-' + response.table[i].id + 
                        '" value=2> В </label></input>');
                    var _radioOptionD = $('<label class="radio-inline"><input type="radio" name="optionsAnswers-' + response.table[i].id + 
                        '" value=3> Г </label></input>');

                    _td.append([
                        _radioOptionA,
                        _radioOptionB,
                        _radioOptionC,
                        _radioOptionD
                    ]);

                    _tableBody.find('#table-question-row-' + response.table[i].id).append(_td);
                }
                                
            }

            table.mainCont.append('<button type="submit" class="btn btn-inverse btn-block">Завершить тестирование</button>');

            $('body').append(table.mainCont); 

        };

        table.constructionSubjects = function(response) {

            table.mainCont = _tableTemplate();

            var _divContainer = table.mainCont.find('div');
            var _table = table.mainCont.find('table');
            var _tableBody = table.mainCont.find('tbody');

            for (var i = 0; i < response.table.length; i++) {

                    _tableBody.append('<tr><td>' + response.table[i].id + 
                        '</td><td>' + response.table[i].name + '</td></tr>');

                }

                $('body').append(table.mainCont);
        };

        table.init();

        return table;
    };

    return _tableModule;
});
