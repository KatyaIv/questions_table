define(
    ['../template/main_native_bar', '../class/questions', '../class/subjects', '../template/main_del', 
     '../template/main_add', '../template/main_edit','class/client', '../template/main_seminar', '../template/main_result'],
    function (_navBarTemplate, _questionModule, _subjModule, _delTemplate, _addTemplate, _editTemplate, _clientModule,
    		 _checkSeminarTemplate, _resultTemplate) {

        var _navBarModule = (function () {
            var navBar = {
                formNavBar: false,
                tableQuestion: false,
                btnGetTableQuestion: false,
                btnDelQuestion: false,
                btnEditQuestion: false,
                btnAddQuestion: false,
                tableSubj: false,
                btnGetTableSubj: false,
                btnDelSubj: false,
                btnEditSubj: false,
                btnAddSubj: false,
                btnExit: false,
                btnSeminarOne: false,
                btnSeminarSeveral: false,
                btnStatistic: false,
                studentList: []
            };

            navBar.init = function (fullName, role) {

                navBar.formNavBar = _navBarTemplate();

                if (navBar.formNavBar.length == 0) return false;

                navBar.formNavBar.find('.navbar-text').append(fullName);

                navBar.tableQuestion = _questionModule();
                navBar.btnGetTableQuestion = $(navBar.formNavBar.find('.get-question'));
                navBar.btnDelQuestione = $(navBar.formNavBar.find('.del-question'));
                navBar.btnEditQuestion = $(navBar.formNavBar.find('.edit-question'));
                navBar.btnAddQuestion = $(navBar.formNavBar.find('.add-question'));

                navBar.tableSubj = _subjModule();
                navBar.btnGetTableSubj = $(navBar.formNavBar.find('.get-subj'));
                navBar.btnDelSubj = $(navBar.formNavBar.find('.del-subj'));
                navBar.btnEditSubj = $(navBar.formNavBar.find('.edit-subj'));
                navBar.btnAddSubj = $(navBar.formNavBar.find('.add-subj'));
                navBar.btnExit = $(navBar.formNavBar.find('.exit'));

                navBar.btnSeminarOne = $(navBar.formNavBar.find('.one-subject'));
                navBar.btnSeminarSeveral = $(navBar.formNavBar.find('.several-subjects'));
                navBar.btnStatistic = $(navBar.formNavBar.find('.get-statistic'));

                navBar.permissions(fullName, role);
                navBar.attachEvent(role);

                $('body').append(navBar.formNavBar);

                return true;
            };

            navBar.permissions = function (fullName, role) {

                if (role == 1) {

                    navBar.formNavBar.find('.navbar-text').append(' (преподаватель)');
                    navBar.formNavBar.find('a').removeClass('disabled');

                } else {

                    navBar.formNavBar.find('.navbar-text').append(' (студент)');
                    navBar.formNavBar.find('.scenario').remove();
                    navBar.formNavBar.find('.get-table').remove();

                    var client = _clientModule();

                    var _data = {
                    	query: 'seminar_started_or_not'
                    }
                    
                    client.post(_data, function(){

                    		var _querySeminar = setInterval(function(){

                    			if (client.responseJson.message == 'Started seminar not found'){

                    				client.post(_data);

                    			} else {         

                    				navBar.startSeminar(client.responseJson.id, client.responseJson.type, client.responseJson.entity);

                    				clearInterval(_querySeminar);

                    			}
                    			
							}, 3000);

                    });
                   
                }

            };

            navBar.attachEvent_GetTableQuestion = function () {

                navBar.btnGetTableQuestion.on('click', function () {

                    if ($('body').find('form').length != 0) {
                        $('body').find('form').remove();
                    }

                    navBar.tableQuestion.getTable('1', 'seminar');

                });

            };

            navBar.attachEvent_DelQuestion = function () {

                navBar.btnDelQuestione.on('click', function () {

                    var _delForm = _delTemplate();
                    var _numSubj;

                    if ($('body').find('form').length != 0) {
                        $('body').find('form').remove();
                    }

                    _delForm.find('h4').text("Удаление вопроса из таблицы");
                    _delForm.find('.control-label').text("Введите номер темы");

                    $('body').append(_delForm);

                    navBar.tableSubj.getTable();

                    _delForm.submit( function () {
                		if(_numSubj === undefined){

                			_numSubj = _delForm.find('input').val();

                    		_delForm.find('.control-label').text("Введите номер вопроса");

                    		$('body').find('.formTable').remove();

                    		_delForm.find('input').val('');

                    		navBar.tableQuestion.getTable(_numSubj);

                		} else {

                			var numDelQuestion = _delForm.find('input').val();
                     
	                        $('body').find('.formTable').remove();

	                        _delForm.find('input').val('');

	                        navBar.tableQuestion.delQuestion(numDelQuestion, _numSubj, function(){
	                        	navBar.tableQuestion.getTable(_numSubj);
	                        });

                		}
                		
                		return false;
                	});

                });

            };

            navBar.attachEvent_AddQuestion = function () {

                navBar.btnAddQuestion.on('click', function () {

                    var _addForm = _addTemplate();
                    var _numSubj;

                    if ($('body').find('form').length != 0) {
                        $('body').find('form').remove();
                    }

                    _addForm.find('h4').text("Добавление вопроса в таблицу");
                  	_addForm.find('.control-label').text("Введите номер темы");
                  	_addForm.find(':input[placeholder]').hide();
                  	_addForm.find('textarea').hide();
                  	_addForm.find('p').hide();
                  	_addForm.find('.radio-inline').hide();

                    $('body').append(_addForm);

                    navBar.tableSubj.getTable();

                	_addForm.submit( function () {
                		if(_numSubj === undefined){

                			_numSubj = _addForm.find('#main-add-inputSubject').val();

                    		_addForm.find('.control-label').text("Введите текст вопроса");
                    		_addForm.find('#main-add-inputSubject').hide();
                    		_addForm.find(':input[placeholder]').show();
                    		_addForm.find('textarea').show();
                    		_addForm.find('p').show();
                  			_addForm.find('.radio-inline').show();

                    		$('body').find('.formTable').remove();

                    		_addForm.find('#main-add-inputSubject').val('');

                    		navBar.tableQuestion.getTable(_numSubj);

                		} else {

                			var _textAddQuestion = _addForm.find('textarea').val();
                			var _answerA = _addForm.find(':input[placeholder="Вариант ответа А"]').val();
                			var _answerB = _addForm.find(':input[placeholder="Вариант ответа Б"]').val();
                			var _answerC = _addForm.find(':input[placeholder="Вариант ответа В"]').val();
                			var _answerD = _addForm.find(':input[placeholder="Вариант ответа Г"]').val();

                			var _numCorrectAnswer = _addForm.find('input[name=optionsAnswers]:checked').val();

                        	$('body').find('.formTable').remove();

                        	_addForm.find(':input[placeholder]').val('');
                        	_addForm.find('textarea').val('');

                        	navBar.tableQuestion.addQuestion(_textAddQuestion, _numSubj, _answerA, _answerB, _answerC, _answerD,
                        	    _numCorrectAnswer, function(){
                        		navBar.tableQuestion.getTable(_numSubj);
                    		});

                		}
                		
                		return false;
                	});

                });

            };

            navBar.attachEvent_EditQuestion = function () {

                navBar.btnEditQuestion.on('click', function () {

                    var _editForm = _editTemplate();
                    var _numSubj;

                    if ($('body').find('form').length != 0) {
                        $('body').find('form').remove();
                    }

                    _editForm.find('h4').text("Редактирование вопросов");
                    _editForm.find('.control-label').text("Введите номер темы");
                    _editForm.find('textarea').hide();
                    _editForm.find(':input[placeholder]').hide();
                    _editForm.find('p').hide();
                    _editForm.find('.radio-inline').hide();

                    $('body').append(_editForm);

                    navBar.tableSubj.getTable();

                    _editForm.submit( function () {
						if(_numSubj === undefined){

                			_numSubj = _editForm.find('#main-edit-inputId').val();

                    	    _editForm.find('.control-label').text("Введите номер вопроса");
                    	    _editForm.find('textarea').show();
                    	    _editForm.find(':input[placeholder]').show();
                    	    _editForm.find('p').show();
                    		_editForm.find('.radio-inline').show();

                    		$('body').find('.formTable').remove();

                    		_editForm.find('#main-edit-inputId').val('');

                    		navBar.tableQuestion.getTable(_numSubj);

                		} else {

	                		var _idQuestion = _editForm.find('#main-edit-inputId').val()
	                        var _textQuestion = _editForm.find('textarea').val();
	                        var _numCorrectAnswer = _editForm.find('input[name=optionsAnswers]:checked').val();
                			
                			var _answerA = _editForm.find(':input[placeholder="Вариант ответа А"]').val();
                			var _answerB = _editForm.find(':input[placeholder="Вариант ответа Б"]').val();
                			var _answerC = _editForm.find(':input[placeholder="Вариант ответа В"]').val();
                			var _answerD = _editForm.find(':input[placeholder="Вариант ответа Г"]').val();

	                        $('body').find('.formTable').remove();

	                        _editForm.find(':input[placeholder]').val('');
	                        _editForm.find('textarea').val('');

	                        navBar.tableQuestion.editQuestion(_idQuestion, _textQuestion, _numSubj, _answerA, _answerB, _answerC, _answerD,
                        	    _numCorrectAnswer,function() {
	                        	navBar.tableQuestion.getTable(_numSubj);
	                        });

                		}
                                                               
                        return false;
                    });


                });

            };

            navBar.attachEvent_GetTableSubj = function () {

                navBar.btnGetTableSubj.on('click', function () {

                    if ($('body').find('form').length != 0) {
                        $('body').find('form').remove();
                    }

                    navBar.tableSubj.getTable();

                });

            };

            navBar.attachEvent_DelSubject = function () {

                navBar.btnDelSubj.on('click', function () {

                    var _delForm = _delTemplate();

                    if ($('body').find('form').length != 0) {
                        $('body').find('form').remove();
                    }

                    _delForm.find('h4').text("Удаление темы из таблицы");
                    _delForm.find('.control-label').text("Введите номер темы");

                    $('body').append(_delForm);

                    navBar.tableSubj.getTable();

                    _delForm.submit( function () {

                        var numDelSubj = _delForm.find('input').val();

                        $('body').find('.formTable').remove();

                        _delForm.find('input').val('');

                        navBar.tableSubj.delSubject(numDelSubj, function(){
                        	navBar.tableSubj.getTable();
                        });                        

                        return false;
                    });

                });

            };

            navBar.attachEvent_AddSubject = function () {

                navBar.btnAddSubj.on('click', function () {

                    var _addForm = _addTemplate();

                    if ($('body').find('form').length != 0) {
                        $('body').find('form').remove();
                    }

                    _addForm.find('h4').text("Добавление темы в таблицу");
                    _addForm.find('.control-label').text("Введите название темы");
                    _addForm.find(':input[placeholder]').hide();
                    _addForm.find('textarea').hide();
                    _addForm.find('p').hide();
                    _addForm.find('.radio-inline').hide();

                    $('body').append(_addForm);

                    navBar.tableSubj.getTable();

                    _addForm.submit( function () {

                        var nameSubj = _addForm.find('input').val();

                        $('body').find('.formTable').remove();

                        _addForm.find('input').val('');

                        navBar.tableSubj.addSubject(nameSubj, function(){
                        	navBar.tableSubj.getTable();
                        });

                        return false;
                    });

                });

            };

            navBar.attachEvent_EditSubject = function () {

                navBar.btnEditSubj.on('click', function () {

                    var _editForm = _editTemplate();

                    if ($('body').find('form').length != 0) {
                        $('body').find('form').remove();
                    }

                    _editForm.find('h4').text("Редактирование тем");
                    _editForm.find('.control-label').text("Введите номер темы");
                    _editForm.find(':input[placeholder]').hide();
                    _editForm.find('p').hide();
                    _editForm.find('.radio-inline').hide();

                    $('body').append(_editForm);

                    navBar.tableSubj.getTable();

                    _editForm.submit( function () {

                        var idSubj = _editForm.find('input').val();

                        var nameSubj = _editForm.find('textarea').val();

                        $('body').find('.formTable').remove();

                        _editForm.find('input').val('');
                        _editForm.find('textarea').val('');

                        navBar.tableSubj.editSubject(idSubj, nameSubj, function(){
                        	navBar.tableSubj.getTable();
                        });

                        return false;
                    });


                });

            };

            navBar.attachEvent_Exit = function() {
             	navBar.btnExit.on('click', function () {

                    	var client = _clientModule();

                    	var _data = {
                    		query: 'exit'
                    	};

                    	client.post(_data);
                    	location.reload();

                    });
            };

            navBar.attachEvent_SeminarOne = function() {

            	navBar.btnSeminarOne.on('click', function () {

            		var _checkSeminarForm = _checkSeminarTemplate();
            		var client = _clientModule();

            		var _numSubj;

                    if ($('body').find('form').length != 0) {
                        $('body').find('form').remove();
                    }

                    _checkSeminarForm.find('h4').text("Выбор темы для проведения тестирования");
                    _checkSeminarForm.find('.control-label').text("Введите номер темы");

                    $('body').append(_checkSeminarForm);

                    navBar.tableSubj.getTable();

                    _checkSeminarForm.submit( function () {
						if(_numSubj === undefined){

	                        _numSubj = _checkSeminarForm.find('input').val();        

	            			var _data = {
	            				query: 'seminar_one_subject',
	            				subject: _numSubj,
	            				type: 1
	            			};

	            			client.post(_data);
	                        
	                        _checkSeminarForm.find('h4').remove();
	                        _checkSeminarForm.find('.control-label').remove();
	                        _checkSeminarForm.find('input').remove();
	                        _checkSeminarForm.find('button').text('Закрыть сценарий');


	                        $('body').find('.formTable').remove();

						} else {

							_data = {
                        		query: 'end_scenario'
                        	};

                        	client.post(_data);

                        	_checkSeminarForm.remove();
						}
                        
                        return false;
                    });
            	});
            };

            navBar.attachEvent_GetStatistic = function( role ) {

				navBar.btnStatistic.on('click', function () {

            		var client = _clientModule();

                    if ($('body').find('form').length != 0) {
                        $('body').find('form').remove();
                    }

	                var _resultForm = _resultTemplate();

		            _resultForm.find('h4').text("Результаты тестирований");

		            var _divCol = _resultForm.find('.col-lg-6');

                    if( role == 0) {
                    	_data = {
                        	query: 'get_my_statistic'
                    	};

	                    client.post(_data, function() {

	                    	for (var i = 0; i < client.responseJson.result.length; i++) {

	                    		if(client.responseJson.result[i].message != 'Not data about this seminar')
	                    		{

			            			var _percentCorrectAnswers = 
			            				Math.round((client.responseJson.result[i].numCorrectAnswers / 
			            					client.responseJson.result[i].numAnswers * 100) * 100 / 100);	               

									_divCol.append('<p><strong>' + client.responseJson.result[i].subjectName + ': </strong> ' + 
										_percentCorrectAnswers +'% (' + client.responseJson.result[i].numCorrectAnswers + ' из ' + 
										client.responseJson.result[i].numAnswers + ')</p><br>');

									$('body').append(_resultForm);
								} 
	                    	}
	                    	
	                    });

                    } else {

                    	var _data = {
                    		query: 'get_student_list'
                    	};

                    	client.post(_data, function(){

                    		navBar.studentList = client.responseJson.studentList;

                    		for(var j = 0; j < navBar.studentList.length; j++)
                    		{
                    			_data = {
                    				query: 'get_statistic_by_studentId',
                    				studentId: navBar.studentList[j]['id'],
                    				numStudent: j
                    			};         
                    		                   		
								client.post(_data, function() {
			       
			       					_divCol.append('<p class="lead">' + navBar.studentList[client.responseJson.numStudent]['full_name'] +
			       						'</p>');

			                    	for (var i = 0; i < client.responseJson.result.length; i++) {

			                    		if(client.responseJson.result[i].message != 'Not data about this seminar')
			                    		{

					            			var _percentCorrectAnswers = 
					            				Math.round((client.responseJson.result[i].numCorrectAnswers / 
					            					client.responseJson.result[i].numAnswers * 100) * 100 / 100);	               

											_divCol.append('<p><strong>' + 
												client.responseJson.result[i].subjectName + ': </strong> ' + _percentCorrectAnswers +'% (' + 
												client.responseJson.result[i].numCorrectAnswers + ' из ' + 
												client.responseJson.result[i].numAnswers + ')</p>');

											$('body').append(_resultForm);
										} 
			                    	}
		                    	});
	                    	};

                    	});                    	               	               
                    }
                                                          
            	});
            };

            navBar.attachEvent = function( role ) {

             	navBar.attachEvent_GetTableQuestion();
                navBar.attachEvent_DelQuestion();
                navBar.attachEvent_DelSubject();
                navBar.attachEvent_GetTableSubj();
                navBar.attachEvent_AddQuestion();
                navBar.attachEvent_AddSubject();
                navBar.attachEvent_EditQuestion();
                navBar.attachEvent_EditSubject();
                navBar.attachEvent_Exit();

                navBar.attachEvent_SeminarOne();
                navBar.attachEvent_GetStatistic( role );

            };

            navBar.startSeminar = function (_seminarId, _type, _entity) {

            	var client = _clientModule();

            	if(_type == '1'){

            		if ($('body').find('form').length != 0) {
                        $('body').find('form').remove();
                    }

            		navBar.tableQuestion.getTable(_entity,  'seminar');

            		$('body').submit(function(){

            			for(var i=0;i< ($('tr').length)/3;i++){
            				
            				var _tr = $('tr:eq(' + i * 3 + ')');
            				var _trId = _tr.attr('id');
            				var _questionId = _trId.substr(_trId.lastIndexOf('-') + 1);            				
            				var _answer = _tr.find('input[name=optionsAnswers-' + _questionId + ']:checked').val();

            				var _data = {
	            				query: 'student_answer',
	            				questionId: _questionId,
	            				answer: _answer,
	            				seminarId: _seminarId
	            			};

	            			client.post(_data);
            			}

            			var _data = {
	            			query: 'result',
	            			seminarId: _seminarId,
	            			subjectId: _entity
	            		};

	            		client.post(_data, function(){

	            			var _resultForm = _resultTemplate();

	            			_resultForm.find('h4').text("Результат тестирования");

	            			var _divCol = _resultForm.find('.col-lg-6');

	            			var _percentCorrectAnswers = client.responseJson.numCorrectAnswers / client.responseJson.numAnswers * 100;

	            			_divCol.append('<p><strong>Тема: </strong>' + client.responseJson.subjectName + '</p><br>');
	            			_divCol.append('<p><strong>Всего ответов: </strong>' + client.responseJson.numAnswers + '</p><br>');
	            			_divCol.append('<p><strong>Правильных ответов: </strong>' + Math.round(_percentCorrectAnswers * 100) / 100 + 
	            				'% (' + client.responseJson.numCorrectAnswers + ' из ' + client.responseJson.numAnswers + ')</p><br>');

	            			 $('body').find('.formTable').remove();

							$('body').append(_resultForm);

	            		});
            			 
            			return false;
            		});

            	} else {

            	}

            };

            return navBar;
        });

        return _navBarModule;
    });