/**
 * Created by Катя on 31-Mar-16.
 */
require(['class/auth', 'class/sign_up', 'class/native_bar', 'class/client'], 
    function (_authModule, _signUpModule, _navBarModule, _clientModule) {

        var client = _clientModule();
        var _data = {
            query: 'auth_or_not'
        };

        client.post(_data, function(){

            if(client.responseJson.message == 'Already auth'){

                var menu = new _navBarModule();
                menu.init(client.responseJson.full_name, client.responseJson.role);

            } else {

                var auth = new _authModule();

                auth.onRegistration(function () {

                    var signUp = new _signUpModule();

                    signUp.init();

                });

                auth.onEnter(function () {

                    var menu = new _navBarModule();

                    menu.init(auth.userFullName, auth.userRole);
                    
                });

                auth.init();
            }

        });
});
