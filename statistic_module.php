<?php

 	class StatisticModule
    {
        function getStudentResult( $userId, $seminarId, $subjectId, $typeResult )
        {
 			global $link;

 			$numCorrectAnswers = 0;
 			$numAnswers = 0;
 			$rowList = [];

            $query = "SELECT question_id,answer_id FROM students_answers WHERE user_id='{$userId}' AND scenario_id='{$seminarId}';";

            if ($result = $link->query($query)) {

            		while ($row = $result->fetch_assoc()) {
	                    $rowList[] = $row;
	                }

	                $numAnswers = count($rowList);

	                if($numAnswers != 0)
	                {
	                	for($i = 0; $i < $numAnswers; $i++)
						{
			                $correctAnswerId = QuestionModule::getAnswerIdByQuestionId( $rowList[$i]['question_id'] );

			                if($rowList[$i]['answer_id'] == $correctAnswerId)
			                {
			                	$numCorrectAnswers++;
			                }
		                        
			            }

			            $subjectName = SubjectModule::getNameById( $subjectId );

			            $answer['status'] = 'OK';
				        $answer['message'] = 'Result are calculated';
				        $answer['numCorrectAnswers'] = $numCorrectAnswers;
				        $answer['numAnswers'] = $numAnswers;
				        $answer['subjectName'] = $subjectName;

			            if($typeResult == 0)
			            {
				            echo json_encode($answer);
			            } else {

			            	return $answer;
			            }
	                
		            
            	} else {

            		$answer['message'] = 'Not data about this seminar';
            		return  $answer;
            	}
	                
            } else {

                if($typeResult == 0)
		        {
	                $answer['status'] = 'ERROR';
	                $answer['message'] = 'The result is not counted';
	 
	 				echo json_encode($answer);
 				}
            }

            $result->free();
            
        }
    }

?>