<?php
    include 'config.php';
    include 'question_module.php';
    include 'subject_module.php';
    include 'seminar_module.php';
    include 'answer_module.php';
    include 'statistic_module.php';
    session_start();

    $request = $_REQUEST;
    $link = new mysqli( $hostname, $username, "", $dbName );

    if ($link->connect_error) {
        die('Ошибка подключения (' . $link->connect_errno . ') '. $link->connect_error);
    }

    $link->query("SET NAMES utf8");

    /*Авторизация*/
    if($request['query'] == 'user_verif')
    {

        $login = $request['login'];
        $password = md5($request['password']);
        $query = "SELECT * FROM users WHERE login = '{$login}'";
        $answer = [];

        if ($result = $link->query($query)) {

            if($row = $result->fetch_assoc()){

                if($password == $row["password"]){

                    $_SESSION['auth'] = true;
                    $_SESSION['role'] = $row['role'];
                    $_SESSION['user_id'] = $row['id'];
                    $_SESSION['full_name'] = $row['full_name'];
                    $answer['status'] = "OK";
                    $answer['message'] = "Auth complete";
                    $answer['role'] = $row["role"];
                    $answer['full_name'] = $row["full_name"];

                    echo json_encode($answer);

                } else {

                    $answer['status'] = "ERROR";
                    $answer['message'] = "Password incorrect";

                    echo json_encode($answer);

                }

            } else {

                $answer['status'] = "ERROR";
                $answer['message'] = "Login incorrect";

                echo json_encode($answer);

            }

            $result->free();
        }

    }

    /*Регистрация*/
    if ($request['query'] == 'reg_user') 
    {

        $login = $request['login'];
        $password = md5($request['password']);
        $full_name = $request['fullName'];
        $role = $request['role'];
        $answer = [];
        $query = "SELECT * FROM users WHERE login='{$login}';";

        if ($result = $link->query($query)) {

            if ($row = $result->fetch_assoc()) {

                if ($login == $row['login']) {

                    $answer['status'] = 'ERROR';
                    $answer['message'] = 'Login busy';

                    echo json_encode($answer);
                } 

                $result->free();

            } else {

                $query = "INSERT INTO users (login, full_name, role, password)" .
                         "VALUES('{$login}', '{$full_name}', '{$role}', '{$password}');";

                if($link->query($query)){

                    $answer['status'] = 'OK';
                    $answer['message'] = 'Registration complete';

                    echo json_encode($answer);

                } else {

                    $answer['status'] = 'ERROR';
                    $answer['message'] = 'Registration is not completed';

                    echo json_encode($answer);

                }    

            }

        }

    }

    if(isset($_SESSION['auth']) && $_SESSION['auth'] === true)
    {
        /*Проверка авторизованности пользователя*/
        if($request['query'] == 'auth_or_not'){

            $answer['status'] = "OK";
            $answer['message'] = "Already auth";
            $answer['role'] = $_SESSION['role'];
            $answer['full_name'] = $_SESSION['full_name'];

            echo json_encode($answer);
        }

        /*Проверка пришла ли таблица с вопросами от преподавателя*/
        if($request['query'] == 'seminar_started_or_not')
        {
            SeminarModule::checkStartedSeminar();
        }

        /*Вывод таблицы questions*/
        if($request['query'] == 'get_table_questions')
        {
            
            $rowList = QuestionModule::getAllBySubjectID( $request['subject'] );

            if( count( $rowList) != 0 )
            {
                $answer['status'] = "OK";
                $answer['table'] = $rowList;

                echo json_encode($answer);

            } else {

                $answer['status'] = "Empty";

                echo json_encode($answer);
            }

        }

        /*Вывод вариантов ответа*/
        if($request['query'] == 'get_answers')
        {
            
            $rowList = AnswerModule::getAllByQuestionID( $request['question']);
            
            if( count( $rowList) != 0 )
            {
                $answer['status'] = "OK";
                $answer['answers'] = $rowList;

                echo json_encode($answer);

            } else {

                $answer['status'] = "Empty";

                echo json_encode($answer);
            }

        }

        /*Запись ответов студента*/
        if($request['query'] == 'student_answer')
        {            
            $rowList = SeminarModule::insertStudentAnswer( $_SESSION['user_id'], $request['questionId'], $request['answer'], 
                $request['seminarId'] );
        }

        /*Вывод результата*/
        if($request['query'] == 'result')
        {
            StatisticModule::getStudentResult( $_SESSION['user_id'], $request['seminarId'], $request['subjectId'], 0 );
        }

        /*Запрос статистики*/
        if($request['query'] == 'get_my_statistic')
        {
            $seminarId = SeminarModule::getIdSeminars();

            for($i = 0; $i < count($seminarId); $i++)
            {
                $resultList[$i] = StatisticModule::getStudentResult( $_SESSION['user_id'], $seminarId[$i]['id'], $seminarId[$i]['entity'], 1 );
            }

            $answer['status'] = "OK";
            $answer['result'] = $resultList;

            echo json_encode($answer);
        }


        if($_SESSION['role'] == '1')
        {
            /*Сценарий семинаров - "Все вопросы по выбранной теме"*/
            if($request['query'] == 'seminar_one_subject')
            {
                SeminarModule::startingSeminar( $request['type'], $request['subject'] );
            }

            /*Завершение семинара*/
            if($request['query'] == 'end_scenario')
            {
                SeminarModule::endingSeminar();
            }

            /*Запрос статистики на всех студентов*/
            if($request['query'] == 'get_statistic_by_studentId')
            {
                $seminarId = SeminarModule::getIdSeminars();

                for($i = 0; $i < count($seminarId); $i++)
                {
                    $resultList[$i] = StatisticModule::getStudentResult( $request['studentId'], $seminarId[$i]['id'], $seminarId[$i]['entity'], 1 );
                }

                $answer['status'] = "OK";
                $answer['result'] = $resultList;
                $answer['numStudent'] = $request['numStudent'];

                echo json_encode($answer);
                
            }

            /*Запрос списка студентов*/
            if($request['query'] == 'get_student_list')
            {
                $query = 'SELECT id,full_name FROM users WHERE role=0;';

                if ($result = $link->query($query)) 
                {
                    while ($row = $result->fetch_assoc()) {
                        $rowList[] = $row;
                    }

                    $answer['studentList'] = $rowList;
                }

                echo json_encode($answer);
            }

            /*Добавление вопроса в таблицу questions*/
            if ($request['query'] == 'insert_question') 
            {
                QuestionModule::insertQuestion( $request['question'], $request['subject'], $request['answerA'], 
                    $request['answerB'], $request['answerC'], $request['answerD'], $request['numCorrectAnswer'] );   
            }

            /*Удаление вопроса из таблицы questions*/
            if ($request['query'] == 'del_question') 
            {
                QuestionModule::delQuestionByID( $request['id'], $request['subject'] );     
            }

            /*Изменение вопроса*/ 
            if ($request['query'] == 'edit_question') 
            {
                QuestionModule::editQuestion( $request['id'], $request['text'], $request['subject'], $request['answerA'], 
                    $request['answerB'], $request['answerC'], $request['answerD'], $request['numCorrectAnswer'] );                         
            }

            /*Вывод таблицы subjects*/
            if($request['query'] == 'get_table_subj')
            {

                $rowList = SubjectModule::getAll();

                if( count( $rowList) )
                {
                    $answer['status'] = "OK";
                    $answer['table'] = $rowList;

                    echo json_encode($answer);
                } else {
                    $answer['status'] = "Empty";

                    echo json_encode($answer);
                }

            }

            /*Добавление вопроса в таблицу subjects*/
            if ($request['query'] == 'insert_subj') 
            {
                SubjectModule::insertNewSubject( $request['subject'] ) ;
            }

            /*Удаление вопроса из таблицы subjects*/
            if ($request['query'] == 'del_subj') 
            {
                SubjectModule::delSubjectByID( $request['id'] ) ;                            
            }

            /*Изменение темы*/ 
            if ($request['query'] == 'edit_subj') 
            {
                SubjectModule::editSubject( $request['id'], $request['name'] );
            }
        }
        

        /*Выход*/
        if($request['query'] == 'exit')
        {
            $answer['status'] = 'OK';
            $answer['message'] = 'Session exit';
            echo json_encode($answer);

            unset($_SESSION['auth']);
            session_destroy();
        }

    } else{

        if($request['query'] == 'auth_or_not')
        {
                $answer['status'] = "OK";
                $answer['message'] = "Пользователь не авторизирован";

                echo json_encode($answer);
        }
    }
   
    $link->close();

?>