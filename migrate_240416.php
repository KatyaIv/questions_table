<?php
    include 'config.php';

    $link = new mysqli($hostname, $username, "", $dbName);

    if ($link->connect_error) {
                die('Ошибка подключения (' . $link->connect_errno . ') '. $link->connect_error);
    }

    $link->query("CREATE TABLE `scenario` (
				  `id` int(11) NOT NULL,
				  `status_id` int(11) NOT NULL COMMENT '0 - активный сценарий, 1 - завершенный сценарий',
				  `type_id` int(11) NOT NULL COMMENT '0 - Вопрос, 1 - Тема',
				  `entity_id` int(11) NOT NULL
				  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

    $link->query("ALTER TABLE `scenario`
				  ADD PRIMARY KEY (`id`);");

    $link->query("ALTER TABLE `scenario`
				  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;");

    $link->close();
?>