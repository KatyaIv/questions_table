<?php
    include 'config.php';

    $link = new mysqli($hostname, $username, "", $dbName);

    if ($link->connect_error) {
                die('Ошибка подключения (' . $link->connect_errno . ') '. $link->connect_error);
    }

    $link->query("DROP table answers");
	$link->query("DROP table students_answers");

    $link->close();
?>