<?php
    include 'config.php';

    $link = new mysqli($hostname, $username, "", $dbName);

    if ($link->connect_error) {
                die('Ошибка подключения (' . $link->connect_errno . ') '. $link->connect_error);
    }

    $link->query("CREATE TABLE `questions` (
                  `id` int(11) NOT NULL,
                  `text` varchar(255) NOT NULL,
                  `subject_id` int(11) NOT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

    $link->query("INSERT INTO `questions` (`id`, `text`, `subject_id`) VALUES
                  (1, 'question1', 1),
                  (2, 'question2', 1);");

    $link->query("ALTER TABLE `questions`
                  ADD PRIMARY KEY (`id`),
                  ADD KEY `subject_id` (`subject_id`);");

    $link->query("ALTER TABLE `questions`
                  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;");

    $link->query("CREATE TABLE `users` (
                  `id` int(11) NOT NULL,
                  `login` varchar(100) NOT NULL,
                  `full_name` varchar(100) NOT NULL,
                  `role_id` tinyint(1) NOT NULL,
                  `password` varchar(255) NOT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

    $link->query("INSERT INTO `users` (`id`, `login`, `full_name`, `role_id`, `password`) VALUES
                  (1, 'test', 'test', 1, '81dc9bdb52d04dc20036dbd8313ed055'),
                  (2, 'Ñ‚ÐµÑÑ‚', 'Ñ‚ÐµÑÑ‚', 0, '81dc9bdb52d04dc20036dbd8313ed055');");

    $link->query("ALTER TABLE `users`
                  ADD PRIMARY KEY (`id`);");

    $link->query("ALTER TABLE `users`
                  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;")

    $link->query("CREATE TABLE `subjects` (
                  `id` int(11) NOT NULL,
                  `name` varchar(255) NOT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

    $link->query("INSERT INTO `subjects` (`id`, `name`) VALUES
                  (1, 'subject 1');");

    $link->query("ALTER TABLE `subjects`
                  ADD PRIMARY KEY (`id`);");

    $link->query("ALTER TABLE `subjects`
                  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;");

    $link->close();
?>