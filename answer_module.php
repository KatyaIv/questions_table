<?php

	class AnswerModule
	{
		function insertAnswers( $questionId, $arrayAnswers, $numCorrectAnswer )
		{
			global $link;

			for($i = 0; $i < 4; $i++)
			{
				$query = "INSERT INTO answers (question_id,title) VALUES('{$questionId}','{$arrayAnswers[$i]}');";

				if($link->query($query))
				{
					if($i == $numCorrectAnswer)
					{
						$idCorrectAnswer = AnswerModule::getIdLastAnswer();
					}
				}
				
			}

			return $idCorrectAnswer['id'];			
		}

		function editAnswers( $questionId, $arrayAnswers, $numCorrectAnswer )
		{
			global $link;

			$rowList = AnswerModule::getAllByQuestionID( $questionId );

			for($i = 0; $i < 4; $i++)
			{
				if($arrayAnswers[$i] != '')
				{
					$query = "UPDATE answers SET title = '{$arrayAnswers[$i]}' WHERE id = '{$rowList[$i]['id']}';";

					$link->query($query);
				}

				if($i == $numCorrectAnswer)
				{
					$idCorrectAnswer = $rowList[$i]['id'];
				}
			}

			return $idCorrectAnswer;
		}

		function getIdLastAnswer()
		{
			global $link;

			$query = "SELECT id FROM answers ORDER BY id DESC LIMIT 1;";

			if ($result = $link->query($query)) {

                $answerId = $result->fetch_assoc();

                $result->free();

            }

            return $answerId;
		}

		function getAllByQuestionID( $questionId )
		{
			global $link;

            $rowList = [];
            $query = "SELECT * FROM answers WHERE question_id='{$questionId}';";

            if ($result = $link->query($query)) {

                while ($row = $result->fetch_assoc()) {
                    $rowList[] = $row;
                }

                $result->free();
            }

            return $rowList;
		}

	}
?>