<?php
    include 'config.php';

    $link = new mysqli($hostname, $username, "", $dbName);

    if ($link->connect_error) {
                die('Ошибка подключения (' . $link->connect_errno . ') '. $link->connect_error);
    }

    $link->query("CREATE TABLE `answers` (
				`id` int(11) NOT NULL,
				`question_id` int(11) NOT NULL,
				`title` varchar(255) NOT NULL
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

    $link->query("INSERT INTO `answers` (`id`, `question_id`, `title`) VALUES
				(1, 1, 'Да'),
				(2, 1, 'Нет'),
				(3, 1, 'Не один из вариантов'),
				(4, 1, 'бла'),
				(5, 2, 'Да'),
				(6, 2, 'Нет'),
				(7, 2, 'Не один из вариантов'),
				(8, 2, 'кек'),
				(9, 3, 'Да'),
				(10, 3, 'Нет'),
				(11, 3, 'Не один из вариантов'),
				(12, 3, 'азаза'),
				(13, 4, 'Да'),
				(14, 4, 'Нет'),
				(15, 4, 'Не знаю'),
				(16, 4, 'бла'),
				(17, 5, 'Да'),
				(18, 5, 'Нет'),
				(19, 5, 'Не знаю'),
				(20, 5, 'бла');");

    $link->query("ALTER TABLE `answers`
					ADD PRIMARY KEY (`id`),
					ADD KEY `question_id` (`question_id`);");
					
	$link->query("ALTER TABLE `answers`
					MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;");
					
	$link->query("ALTER TABLE `questions` ADD `answer_id` INT NOT NULL AFTER `text`;");
					
	$link->query("INSERT INTO `questions` (`id`, `text`, `answer_id`, `subject_id`) VALUES
				(3, 'question3', 12, 1),
				(4, 'question 4', 13, 2),
				(5, 'question 5', 16, 1);");
					
	$link->query("UPDATE `questions` SET `answer_id` = '2' WHERE `questions`.`id` = 1");
	
	$link->query("UPDATE `questions` SET `answer_id` = '5' WHERE `questions`.`id` = 2");
	
	$link->query("ALTER TABLE `questions` 
					  ADD KEY `answer_id` (`answer_id`);");
					  
	$link->query("ALTER TABLE `questions`
					MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;");

	$link->query("ALTER TABLE `scenario` 
					CHANGE `status_id` `status` INT(11) NOT NULL COMMENT '0 - активный сценарий, 1 - завершенный сценарий', 
					CHANGE `type_id` `type` INT(11) NOT NULL COMMENT '0 - Вопрос, 1 - Тема', 
					CHANGE `entity_id` `entity` INT(11) NOT NULL;");
	
	$link->query("CREATE TABLE `students_answers` (
				  `id` int(11) NOT NULL,
				  `user_id` int(11) NOT NULL,
				  `question_id` int(11) NOT NULL,
				  `answer_id` int(11) DEFAULT NULL,
				  `scenario_id` int(11) NOT NULL
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
				
	$link->query("ALTER TABLE `students_answers`
				  ADD PRIMARY KEY (`id`),
				  ADD KEY `user_id` (`user_id`),
				  ADD KEY `question_id` (`question_id`),
				  ADD KEY `scenario_id` (`scenario_id`),
				  ADD KEY `answer_id` (`answer_id`);");
				  
	$link->query("ALTER TABLE `students_answers`
					MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;");
					
	$link->query("");
	$link->query("");
	$link->query("");
	
    $link->close();
?>