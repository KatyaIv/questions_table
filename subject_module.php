<?php
    class SubjectModule
    {
        function getAll()
        {
            global $link;

            $rowList = [];
            $query = "SELECT * FROM subjects";

            if ($result = $link->query($query)) {

                while ($row = $result->fetch_assoc()) {
                     $rowList[] = $row;
                }

                $result->free();

            }

            return $rowList;
        }

        function insertNewSubject( $subject )
        {
            global $link;

            $answer = [];
            $query = "INSERT INTO subjects (name) VALUES('{$subject}');";

            if($link->query($query)){

                $answer['status'] = 'OK';
                $answer['message'] = 'Insert complete';
                echo json_encode($answer);

            } else {

                $answer['status'] = 'ERROR';
                $answer['message'] = 'Insert is not completed';
                echo json_encode($answer);
            } 
        }

        function delSubjectByID( $id )
        {
            global $link;

            $rowList = SubjectModule::getAll();

            $answer = [];
            $query = "DELETE FROM subjects WHERE `id` = '{$rowList[$id-1]['id']}';";

            if($link->query($query)){

                $answer['status'] = 'OK';
                $answer['message'] = 'Delete complete';
                echo json_encode($answer);

            } else {

                $answer['status'] = 'ERROR';
                $answer['message'] = 'Delete is not completed';
                echo json_encode($answer);

            }     
        }

        function editSubject( $id, $name )
        {
            global $link;

            $rowList = SubjectModule::getAll();
            
            $answer = [];
            $query = "UPDATE subjects SET name = '{$name}' WHERE subjects.id = '{$rowList[$id-1]['id']}';";

            if($link->query($query)){

                $answer['status'] = 'OK';
                $answer['message'] = 'Edit complete';
                echo json_encode($answer);

            } else {

                $answer['status'] = 'ERROR';
                $answer['message'] = 'Edit is not completed';
                echo json_encode($answer);

            }                          
        }

        function getNameById( $subjectId )
        {
            global $link;

            $query = "SELECT name FROM subjects WHERE id = '{$subjectId}';";

            if ($result = $link->query($query)) {

                $subjectName = $result->fetch_assoc();

                $result->free();

            }

            return $subjectName['name'];
        }
    }
?>