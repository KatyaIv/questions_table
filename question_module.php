<?php

    class QuestionModule
    {
        function getAllBySubjectID( $subject )
        {
            global $link;

            $rowList = [];
            $query = "SELECT * FROM questions WHERE subject_id='{$subject}';";

            if ($result = $link->query($query)) {

                while ($row = $result->fetch_assoc()) {
                    $rowList[] = $row;
                }

                $result->free();

            }

            return $rowList;
        }

        function delQuestionByID( $id, $subject )
        {
            global $link;

            $rowList = QuestionModule::getAllBySubjectID($subject);
            $answer = [];

            $query = "DELETE FROM questions WHERE questions.`id` = '{$rowList[$id-1]['id']}';";

            if($link->query($query))
            {

                $queryToAnswers = "DELETE FROM answers WHERE answers.`question_id` = '{$rowList[$id-1]['id']}';";

                if($link->query($queryToAnswers))
                {
                    $answer['status'] = 'OK';
                    $answer['message'] = 'Delete complete';
                    echo json_encode($answer); 

                } else {

                    $answer['status'] = 'ERROR';
                    $answer['message'] = 'Delete is not completed';
                    echo json_encode($answer);
                }
                

            } else {

                $answer['status'] = 'ERROR';
                $answer['message'] = 'Delete is not completed';
                echo json_encode($answer);

            }
        }

        function insertQuestion( $text, $subject, $answerA, $answerB, $answerC, $answerD, $numCorrectAnswer)
        {
            global $link;

            $answer = [];

            $query = "INSERT INTO questions (text, subject_id) VALUES('{$text}','{$subject}');";

            if($link->query($query)){

                $questionId = QuestionModule::getIdByText( $text );

                $arrayAnswers[0] = $answerA;
                $arrayAnswers[1] = $answerB;
                $arrayAnswers[2] = $answerC;
                $arrayAnswers[3] = $answerD;

                $idCorrectAnswer = AnswerModule::insertAnswers( $questionId['id'], $arrayAnswers, $numCorrectAnswer );

                QuestionModule::insertAnswerID( $questionId['id'], $idCorrectAnswer );

                $answer['status'] = 'OK';
                $answer['message'] = 'Insert complete';
                echo json_encode($answer);

            } else {

                $answer['status'] = 'ERROR';
                $answer['message'] = 'Insert is not completed';
                echo json_encode($answer);
            }
        }

        function editQuestion( $id, $text, $subject, $answerA, $answerB, $answerC, $answerD, $numCorrectAnswer )
        {
            global $link;

            $rowList = QuestionModule::getAllBySubjectID($subject);

            $answer = [];
            if( $text != '')
            {
                $query = "UPDATE questions SET text = '{$text}' WHERE questions.id = '{$rowList[$id-1]['id']}';";
            }

            if($link->query($query)){

                $arrayAnswers[0] = $answerA;
                $arrayAnswers[1] = $answerB;
                $arrayAnswers[2] = $answerC;
                $arrayAnswers[3] = $answerD;
                
                $idCorrectAnswer = AnswerModule::editAnswers( $rowList[$id-1]['id'], $arrayAnswers, $numCorrectAnswer );

                QuestionModule::insertAnswerID( $rowList[$id-1]['id'], $idCorrectAnswer );

                $answer['status'] = 'OK';
                $answer['message'] = 'Edit complete';
                echo json_encode($answer);

            } else {

                $answer['status'] = 'ERROR';
                $answer['message'] = 'Edit is not completed';
                echo json_encode($answer);

            }  
        }

        function getIdByText( $text )
        {
            global $link;

            $query = "SELECT id FROM questions WHERE text='{$text}';";

            if ($result = $link->query($query)) {

                $questionId = $result->fetch_assoc();

                $result->free();

            }

            return $questionId;
        }

        function insertAnswerID( $questionId, $answerId )
        {
            global $link;

            $query = "UPDATE questions SET answer_id = '{$answerId}' WHERE questions.id = '{$questionId}';";

            $link->query($query);
        }

        function getAnswerIdByQuestionId( $questionId )
        {
            global $link;

            $query = "SELECT answer_id FROM questions WHERE questions.id = '{$questionId}';";

            if ($result = $link->query($query)) {

                $answerId = $result->fetch_assoc();

                $result->free();

            }

            return $answerId['answer_id'];
        }

    }
?>